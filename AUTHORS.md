# Authors

This is the official list of 9Dragons XSD Editor authors for copyright purposes.

- Beau Hastings ([hastinbe](https://github.com/hastinbe)) aka ([saweet](http://www.elitepvpers.com/forum/members/499233-saweet.html))
- Hunter aka huntercool aka animehunter (Cracked the new 9d XSD format)
- g3nuin3 (With help from this guy)
