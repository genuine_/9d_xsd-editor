﻿//  Copyright (c) 2012 Beau Hastings. All rights reserved.
//  License: GNU GPL version 2, see LICENSE for more details.
//  Author: Beau Hastings <beausy@gmail.com>
using System;

namespace NineDragons_XSD_Editor.Data
{
    public class XsdHeader
    {
        public int Unknown1 = 0xFFCF; // 65487
        public int Unknown2 = 0xFF01; // 65281
        public int NumTables = 0;
    }
}
