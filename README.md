9Dragons XSD Editor
===================

A program for editing XSD files for 9Dragons, a massively multiplayer online role playing game
developed by Indy21.

![9Dragons XSD Editor](https://lh3.googleusercontent.com/-3f_JCMSAeSM/UJPmpCWv3JI/AAAAAAAAAk8/II9H10MgOUk/s752/user499233_pic25356_1316123922.png)
*********
License
-------
GNU GPL v2
Copyright (C) 1989, 1991 Free Software Foundation, Inc.